package ru.t1k.vbelkin.tm.api.repository;

import ru.t1k.vbelkin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Integer getSize();

    Project add(Project project);

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    boolean existsById(String id);

}
