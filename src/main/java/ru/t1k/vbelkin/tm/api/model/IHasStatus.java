package ru.t1k.vbelkin.tm.api.model;

import ru.t1k.vbelkin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
