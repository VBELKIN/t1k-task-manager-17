package ru.t1k.vbelkin.tm.api.model;

import ru.t1k.vbelkin.tm.api.service.*;

public interface IServiceLocator {
    ICommandService getCommandService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    ILoggerService getLoggerService();
}
